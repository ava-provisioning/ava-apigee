# Konvertieren des ASCIIdoctor [adoc] Dokuments in PDF

## Installation von asciidoctor-pdf 

## *(Ruby Gems, Cygwin)*

- Ruby installieren
- Gems installieren
  - `gem install hexapdf`
  - `gem install text-hyphen`
  - `gem install asciidoctor-pdf`
  - `gem install asciidoctor-diagram`
  - `gem install rouge`
- Gemfile erzeugen
```ruby
gem 'asciidoctor-pdf'
gem 'prawn'
gem 'prawn-table'
gem 'rouge'
gem 'asciidoctor-diagram'
```

- `bundle` ausführen (Gemfile compile)

### adoc => PDF

- simple PDF rendern:
  `asciidoctor-pdf -v <quelltext.adoc>`

- PDF rendern mit eingebettetem PlantUML / Diagrammen:
  `asciidoctor-pdf -v -r asciidoctor-diagram <quelltext.adoc>`

- Referenz *asciidoctor-diagram* Plugin: 
  https://www.rubydoc.info/gems/asciidoctor-diagram/1.4.0

- Theming-Guide: 
  https://github.com/asciidoctor/asciidoctor-pdf/blob/v1.6.0/docs/theming-guide.adoc

- theme file must be named `<name>-theme.yml`

- Attribut `:pdf-theme: <theme-yaml>:` in Dokument-Header, 
  z.B. `:pdf-theme: themes/prom-pdf-theme.yml:`

  
