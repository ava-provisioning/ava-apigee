# ava-apigee Project

## apigee-microgateway

### Installation Docker
#### Prerequisites
You must have 
- Node.js version 6.x LTS or 8.x LTS installed on your system. 
- npm

You can check the version of Node.js by executing the following command:
```
node -v
```
#### Installing Edge Microgateway (with internet connection)
1. Install the latest version of Edge Microgateway with npm as follows. This command installs the software and puts the edgemicro executable in your path.
```
sudo npm install edgemicro -g
```
2. Check the version number with:
```
edgemicro --version
```
3. Configuration Edge Microgateway
**Apigee Private Cloud configuration steps**

A. Initialize Edge Microgateway (you only need to do this step one time):
```
edgemicro init
```
A config file called default.yaml file was placed in your home directory in a subdirectory called .edgemicro. 

B. Execute the following command to configure Edge Microgateway:
```
edgemicro private configure -o [org] -e [env] -u [username] -r [runtime_url] -m [mgmt_url] -v [virtual_host]
```

Where:
* org:		Your Edge organization name (you must be an org administrator).
* env:		An environment in your org (such as test or prod).
* runtime_url:	is the runtime URL for your private cloud instance.
* mgmt_url:	is The URL of the management server for your private cloud instance.
* username:	is the email address associated with your Apigee account.
* virtual_host:	is a comma-separated list of virtual host names. The default values are default,secure

**Example**
```
edgemicro private configure -o docs -e test -u jdoe@example.com -r http://192.162.52.106:9001 -m http://192.162.52.106:8080 -v default
```

C. Verify the installation
Run this command to verify the installation. If no errors are reported, everything is set up correctly and you will be able to start the Edge Microgateway successfully.

```
edgemicro verify -o [org] -e [env] -k [key] -s [secret]
```
Where:
* org is your Edge organization name (you must be an org administrator).
* env is an environment in your org (such as test or prod).
* key is the key returned previously by the configure command.
* secret is the key returned previously by the configure command.

**Example**
```
edgemicro verify -o docs -e test -k 93b01fd21d86331459ae52f624ae9aeb13eb94767ce40a4f621d172cdfb7e8e6 -s c8c755be97cf56c21f8b0156d7132afbd03625bbd85dc34ebfefae4f23fbcb3c
```



5. Start des Docker Containers

```
docker run \
  -P -p 8000:8000 -d \
  --name edgemicro \
  -v /home/dev/docker_tmp/edgemicro:/opt/apigee/logs \
  -e EDGEMICRO_PROCESSES=1 \
  -e EDGEMICRO_ORG=<XXX> \
  -e EDGEMICRO_ENV=test \
  -e EDGEMICRO_KEY=<XXX> \
  -e EDGEMICRO_SECRET=<XXX> \
  -e EDGEMICRO_CONFIG=$EDGEMICRO_CONFIG \
  -e SERVICE_NAME=edgemicro \
  --security-opt=no-new-privileges \
  --cap-drop=ALL \
  gcr.io/apigee-microgateway/edgemicro:latest
```



# Literatur
[1] https://docs.apigee.com/
[2] https://docs.apigee.com/api-platform/microgateway/2.5.x/setting-and-configuring-edge-microgateway
